FROM keymetrics/pm2:latest-alpine

WORKDIR /home/mmis  

COPY . .

RUN npm install 
RUN npm run build

# Install app dependencies

# Show current folder structure in logs

CMD [ "pm2-runtime", "start", "pm2.json" ]

EXPOSE 8080
