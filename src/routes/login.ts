/// <reference path="../../typings.d.ts" />

import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as crypto from 'crypto'

const router: Router = Router();

import {LoginModel} from '../models/login' // import Model Login เข้ามา
import {Jwt} from '../models/jwt' //import jwt เข้ามาเพื่อสร้าง token
const loginModel=new LoginModel();  //ประกาศ Model จาก LoginModel
const jwt=new Jwt();  //ประกาศ Model จาก class Jwt

router.post('/', async(req: Request, res: Response) => {  //login
    const username:string=req.body.username;
    const password:string=req.body.password;
try {
    let encPass = crypto.createHash('md5').update(password).digest('hex');
    const rs:any=await loginModel.login(req.db,username,encPass); //ส่งค่าเข้าไป ตรวจสอบใน LoginModel
    if(rs.length){
        let  payload={
            id:rs[0].id,  //เรียก object ที่ 0 หรือคนแรก
            fullname:rs[0].fname
        }
        let token=jwt.sign(payload);
        res.send({ok:true,token:token});
    }else{
        res.send({ok:false,error:"ชื่อผู้ใช้งานไม่ถูกต้อง"});
    }

} catch (error) {
     res.send({ok:false,error:'เกิดข้อผิดพลาด',code:500});
}
  // req.logger.info('test logger');
  res.send({ ok: true, message: 'Welcome to RESTful api server!', code: HttpStatus.OK });
});

export default router;