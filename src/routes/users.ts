/// <reference path="../../typings.d.ts" />

import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as crypto from 'crypto';
import {UserModel} from '../models/user';   //import model เข้ามาเพื่อประกาศ ตัวแปร
const router: Router = Router();
const userModel=new UserModel();

router.get('/detail', async (req: Request, res: Response) => {  //รับค่าเพื่อ  select  data เพิ่มเติม async ไว้ข้างหน้าด้วย
  // req.logger.info('test logger');
  try {
    let rs:any= await userModel.getlist(req.db);   //เรียกใช้ ีuserModel.getlist เพิ่มเติม await เพื่อ รอคำสั่งทำเสร็จก่อน
    res.send({ok:true,rows:rs});

  } catch (error) {
    req.logger.error(error);
    res.send({ok:false,error:'เกิดข้อผิดพลาด',code:500});
  }
  res.send({ ok: true, message: 'GET', code: HttpStatus.OK });
});
router.get('/detail/:id', async (req: Request, res: Response) => {  //รับค่าเพื่อ  select  data เพิ่มเติม async ไว้ข้างหน้าด้วย
  const id:string=req.params.id;
  try {
    let rs:any= await userModel.getlistid(req.db,id); //เรียกใช้ ีuserModel.getlist เพิ่มเติม await เพื่อ รอคำสั่งทำเสร็จก่อน
    res.send({ok:true,rows:rs});

  } catch (error) {
    req.logger.error(error);
    res.send({ok:false,error:'เกิดข้อผิดพลาด',code:500});
  }
  res.send({ ok: true, message: 'GET', code: HttpStatus.OK });
});
router.post('/detail', async(req: Request, res: Response) => { //รับค่าเพื่อ  insert data
  const fname:string=req.body.fname;       //รับค่าจาก body form 
  const lname:string=req.body.lname; 
  const username:string=req.body.username; 
  const password:string=req.body.password; 
  //fullname :'${fname}${lname}'
  //fullname:fname+''+lname;

  try {
    let encPass = crypto.createHash('md5').update(password).digest('hex');
    let data:any={      //data ที่ post เข้ามามี อะไรบ้างใส่เข้าไปให้ตรง
        fname:fname,
        lname:lname,
        username:username,
        password:encPass
    }
    let rs:any= await userModel.save(req.db,data);   //เรียกใช้ ี จะมี db และ data
    res.send({ok:true,rows:rs});

  } catch (error) {
    req.logger.error(error);
    res.send({ok:false,error:'เกิดข้อผิดพลาด',code:500});
  }

  // req.logger.info('test logger');
  res.send({ ok: true, A:fname,B:lname, code: HttpStatus.OK });
});
router.put('/detail/:id', async(req: Request, res: Response) => { //รับค่าเพื่อ  update data
  const id:string=req.params.id;
  const username:string=req.body.username;  
  const password:string=req.body.password;
  const fname:string=req.body.fname;       //รับค่าจาก body form 
  const lname:string=req.body.lname; 
  //fullname :'${fname}${lname}'
  //fullname:fname+''+lname;

  try {
    let encPass = crypto.createHash('md5').update(password).digest('hex');
    let data:any={      //data ที่ post เข้ามามี อะไรบ้างใส่เข้าไปให้ตรง
        password:encPass,
        fname:fname,
        lname:lname,
        username:username
    }
    await userModel.update(req.db,id,data);   //เรียกใช้ ี จะมี db และ data
    res.send({ok:true});

  } catch (error) {
    req.logger.error(error);
    res.send({ok:false,error:'เกิดข้อผิดพลาด',code:500});
  }
  res.send({ ok: true, A:fname,B:lname, code: HttpStatus.OK });
});
router.delete('/detail/:id', async(req: Request, res: Response) => { //รับค่าเพื่อ delete data
 const id:string=req.params.id;
  try {
   if(id){
     await userModel.delete(req.db,id);
     res.send({ok:true})
   }
 } catch (error) {
  res.send({ok:false})
 }
  res.send({ ok: true, message: 'DELETE', code: HttpStatus.OK });
});



router.get('/', (req: Request, res: Response) => {
  // req.logger.info('test logger');
  res.send({ ok: true, message: 'Hello Mr.Sakda intha', code: HttpStatus.OK });
});



router.get('/list', (req: Request, res: Response) => {  //list?a=1&b=2
  // req.logger.info('test logger');
  const a:number=req.query.a;     //const รับค่าได้ครั้งเดียวไม่เปลี่ยนแปลง
  const b:number=req.query.b;

  console.log(a);
  console.log(b);
  res.send({ ok: true, message: a+b, code: HttpStatus.OK });
});

router.get('/info/:a/:b', (req: Request, res: Response) => {  //info/:a/:b  แบบ params
  // req.logger.info('test logger');
  const a:number=req.params.a;     //const รับค่าได้ครั้งเดียวไม่เปลี่ยนแปลง
  const b:number=req.params.b;

  console.log(a);
  console.log(b);
  res.send({ ok: true, A: "A :"+a,B:"B:"+b, code: HttpStatus.OK });
});
export default router;