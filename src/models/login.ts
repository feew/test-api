import * as Knex from 'knex'

export class LoginModel{
    login(db:Knex,user:string,pass:string){
        return db('users')
        .where({username:user,password:pass})
        .limit(1)
    }

}///