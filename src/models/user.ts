import * as Knex from 'knex';
export class UserModel{

getlist(db:Knex){
return db('users').select('id','fname','lname','username','password');

//return  knex.raw("call TEST(:id)",{id:q});
//let sql=`select * from users`;
//return db.raw(sql,[2]);
}
getlistid(db:Knex,id:any){
    return db('users').select('id','fname','lname','username','password').where('id',id);
}
save(db:Knex,data:any){
    return db('users').insert(data);
}
update(db:Knex,id:any,data:any){
    return db('users').update(data).where('id',id);
}
delete(db:Knex,id:any){
    return db('users').where('id',id).del();
}
}