//require('dotenv').config(); //npm i dotenv -S ด้วยถ้าต้องการใช้งาน

import * as path from 'path';
let envPath = path.join(__dirname, '../config');
require('dotenv').config(({ path: envPath }));

import * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import * as HttpStatus from 'http-status-codes';
import * as express from 'express';


import * as Knex from 'knex';  //กำหนด import knex
import  {MySqlConnectionConfig} from 'knex'; //ตั้งชื่อ mysqlconnection
import {Jwt} from './models/jwt'  //import jwt เพื่อที่ทำการ verify จาก fomr url ถูก gen โดยเราไหม

const signale = require('signale');
const rateLimit = require("express-rate-limit");
const helmet = require('helmet');
const cors = require('cors');

import { Router, Request, Response, NextFunction } from 'express';

// Import routings
import indexRoute from './routes/index';
import userRoute from './routes/users';
import loginRoute from './routes/login';
import { decode } from 'jsonwebtoken';

const app: express.Application = express();
const jwt=new Jwt();
const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100 // limit each IP to 100 requests per windowMs
});

var connection:MySqlConnectionConfig={
  host:process.env.DB_HOST,
  port:+process.env.DB_PORT,  // เพิ่ม + convert number to string
  database:process.env.DB_NAME,
  user:process.env.DB_USER,
  password:process.env.DB_PASSWORD
}
var db: Knex = Knex({
  client: 'mysql',
  connection: connection,
  pool: {
    min: 0,
    max: 100,
    afterCreate: (conn, done) => {
      conn.query('SET NAMES utf8', (err) => {
        done(err, conn)
      })
    }
  }
});

let checkAuth=(req:Request,res,next)=>{
  let token='';
  console.log(req.headers);
  if(req.headers.authorization){
   //token=req.headers.authorization.split(' ')[1];  //เชคจาก Bearer ใน header ที่ส่งเข้ามา
   let _auth=req.headers.authorization.split(' '); 
   if(_auth[0]=='Bearer'){
     token=_auth[1];
   }
  }else if(req.query.token){
    token=req.query.token;
  }else if(req.body.token){
    token=req.body.token;
  }else {

  }
  console.log('FEEW:'+token);
  jwt.verify(token).then(decoded=>{  //รับตัวแปร token เข้ามา decode
    req.decoded=decoded;
    next();  //เพื่อให้มันดำเนินการต่อไป ไม่งั้นมันจะหมุนๆ
  }).catch(()=>{
    
    res.send({ok:false,message:HttpStatus.UNAUTHORIZED,
      code:HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED)});
  })
}

app.use((req: Request, res: Response, next: NextFunction) => {
  req.logger = signale;
  next();
});

app.use(limiter);
app.use(helmet({ hidePoweredBy: { setTo: 'PHP 4.2.0' } }));
app.use(cors());

// app.use(logger('dev'));
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

app.get('/favicon.ico', (req: Request, res: Response) => res.status(204));


app.use((req,res,next)=>{
  req.logger.info('hello');
  req.db=db;    //ตัวแปร db จาก typings.d.ts
  next();
})

app.use('/', indexRoute);
app.use('/users', checkAuth,userRoute);  //ทำมันจะทำงาน checkAuth ก่อน
app.use('/login',loginRoute);
//error handlers

if (process.env.NODE_ENV === 'development') {
  app.use((err: any, req: Request, res: Response, next: NextFunction) => {
    req.logger.error(err.stack);
    res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
      error: {
        ok: false,
        code: HttpStatus.INTERNAL_SERVER_ERROR,
        error: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR)
      }
    });
  });
}

app.use((req: Request, res: Response, next: NextFunction) => {
  res.status(HttpStatus.NOT_FOUND).json({
    error: {
      ok: false,
      code: HttpStatus.NOT_FOUND,
      error: HttpStatus.getStatusText(HttpStatus.NOT_FOUND)
    }
  });
});

export default app;
